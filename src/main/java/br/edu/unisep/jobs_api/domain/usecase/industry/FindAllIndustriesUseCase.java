package br.edu.unisep.jobs_api.domain.usecase.industry;

import br.edu.unisep.jobs_api.data.repository.IndustryRepository;
import br.edu.unisep.jobs_api.domain.builder.industry.IndustryBuilder;
import br.edu.unisep.jobs_api.domain.dto.company.CompanyDto;
import br.edu.unisep.jobs_api.domain.dto.industry.IndustryDto;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@AllArgsConstructor
public class FindAllIndustriesUseCase {
    private final IndustryRepository industryRepository;
    private final IndustryBuilder builder;

    public List<IndustryDto> execute() {
        var industries = industryRepository.findAll();
        return builder.from(industries);
    }
}
