package br.edu.unisep.jobs_api.domain.dto.job;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public class JobDto {

    private Integer id;

    private String title;

    private String description;

    private String seniority;

    private String modality;

    private Float salary;

    private String company;
}
