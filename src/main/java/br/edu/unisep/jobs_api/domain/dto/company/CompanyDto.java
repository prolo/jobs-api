package br.edu.unisep.jobs_api.domain.dto.company;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public class CompanyDto {

    private final Integer id;

    private final String name;

    private final String industry;
}
