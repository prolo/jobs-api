package br.edu.unisep.jobs_api.domain.builder.company;

import br.edu.unisep.jobs_api.data.entity.company.Company;
import br.edu.unisep.jobs_api.domain.dto.company.CompanyDto;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.stream.Collectors;

@Component
public class CompanyBuilder {
    public List<CompanyDto> from(List<Company> companies) {
        return companies.stream().map(this::from).collect(Collectors.toList());
    }

    public CompanyDto from(Company company) {
        return new CompanyDto(
                company.getId(),
                company.getName(),
                company.getIndustry().getName()
        );
    }
}
