package br.edu.unisep.jobs_api.domain.builder.industry;

import br.edu.unisep.jobs_api.data.entity.company.Company;
import br.edu.unisep.jobs_api.data.entity.industry.Industry;
import br.edu.unisep.jobs_api.domain.dto.company.CompanyDto;
import br.edu.unisep.jobs_api.domain.dto.industry.IndustryDto;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.stream.Collectors;

@Component
public class IndustryBuilder {

    public List<IndustryDto> from(List<Industry> industries) {
        return industries.stream().map(this::from).collect(Collectors.toList());
    }

    public IndustryDto from(Industry industry) {
        return new IndustryDto(
                industry.getId(),
                industry.getName()
        );
    }
}
