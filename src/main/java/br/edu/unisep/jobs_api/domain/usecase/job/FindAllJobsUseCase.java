package br.edu.unisep.jobs_api.domain.usecase.job;

import br.edu.unisep.jobs_api.data.repository.IndustryRepository;
import br.edu.unisep.jobs_api.data.repository.JobRepository;
import br.edu.unisep.jobs_api.domain.builder.industry.IndustryBuilder;
import br.edu.unisep.jobs_api.domain.builder.job.JobBuilder;
import br.edu.unisep.jobs_api.domain.dto.industry.IndustryDto;
import br.edu.unisep.jobs_api.domain.dto.job.JobDto;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@AllArgsConstructor
public class FindAllJobsUseCase {

    private final JobRepository jobRepository;
    private final JobBuilder builder;

    public List<JobDto> execute() {
        var jobs = jobRepository.findAll();
        return builder.from(jobs);
    }
}
