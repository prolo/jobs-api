package br.edu.unisep.jobs_api.domain.dto.job;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class RegisterJobDto {

    private String title;

    private String description;

    private String seniority;

    private String modality;

    private Float salary;

    private Integer companyId;
}
