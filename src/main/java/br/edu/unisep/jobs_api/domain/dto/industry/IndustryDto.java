package br.edu.unisep.jobs_api.domain.dto.industry;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public class IndustryDto {
    private final Integer id;

    private final String name;
}
