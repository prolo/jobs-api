package br.edu.unisep.jobs_api.domain.usecase.company;

import br.edu.unisep.jobs_api.data.repository.CompanyRepository;
import br.edu.unisep.jobs_api.domain.builder.company.CompanyBuilder;
import br.edu.unisep.jobs_api.domain.dto.company.CompanyDto;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@AllArgsConstructor
public class FindAllCompaniesUseCase {
    private final CompanyRepository companyRepository;
    private final CompanyBuilder builder;

    public List<CompanyDto> execute() {
        var companies = companyRepository.findAll();
        return builder.from(companies);
    }
}
