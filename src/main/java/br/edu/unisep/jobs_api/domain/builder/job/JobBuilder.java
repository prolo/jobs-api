package br.edu.unisep.jobs_api.domain.builder.job;

import br.edu.unisep.jobs_api.data.entity.industry.Industry;
import br.edu.unisep.jobs_api.data.entity.job.Job;
import br.edu.unisep.jobs_api.domain.dto.industry.IndustryDto;
import br.edu.unisep.jobs_api.domain.dto.job.JobDto;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.stream.Collectors;

@Component
public class JobBuilder {

    public List<JobDto> from(List<Job> jobs) {
        return jobs.stream().map(this::from).collect(Collectors.toList());
    }

    public JobDto from(Job job) {
        return new JobDto(
                job.getId(),
                job.getTitle(),
                job.getDescription(),
                job.getSeniority(),
                job.getModality(),
                Float.parseFloat(job.getSalary()),
                job.getCompany().getName()
        );
    }
}
