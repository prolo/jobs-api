package br.edu.unisep.jobs_api.controller.industry;

import br.edu.unisep.jobs_api.domain.dto.industry.IndustryDto;
import br.edu.unisep.jobs_api.domain.usecase.industry.FindAllIndustriesUseCase;
import br.edu.unisep.jobs_api.response.DefaultResponse;
import lombok.AllArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@AllArgsConstructor
@RestController
@RequestMapping("/industry")
public class IndustryController {

    private final FindAllIndustriesUseCase findAllIndustries;

    @GetMapping
    public ResponseEntity<DefaultResponse<List<IndustryDto>>> findAll() {
        var industries = findAllIndustries.execute();

        return industries.isEmpty() ?
                ResponseEntity.noContent().build():
                ResponseEntity.ok(DefaultResponse.of(industries));
    }

}
