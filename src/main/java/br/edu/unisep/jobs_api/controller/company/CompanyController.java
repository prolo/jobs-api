package br.edu.unisep.jobs_api.controller.company;

import br.edu.unisep.jobs_api.domain.dto.company.CompanyDto;
import br.edu.unisep.jobs_api.domain.usecase.company.FindAllCompaniesUseCase;
import br.edu.unisep.jobs_api.response.DefaultResponse;
import lombok.AllArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@AllArgsConstructor
@RestController
@RequestMapping("/company")
public class CompanyController {

    private final FindAllCompaniesUseCase findAllCompanies;

    @GetMapping
    public ResponseEntity<DefaultResponse<List<CompanyDto>>> findAll() {
        var companies = findAllCompanies.execute();

        return companies.isEmpty() ?
                ResponseEntity.noContent().build():
                ResponseEntity.ok(DefaultResponse.of(companies));
    }

}
