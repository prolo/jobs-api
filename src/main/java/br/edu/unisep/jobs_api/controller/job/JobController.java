package br.edu.unisep.jobs_api.controller.job;

import br.edu.unisep.jobs_api.domain.dto.job.JobDto;
import br.edu.unisep.jobs_api.domain.usecase.job.FindAllJobsUseCase;
import br.edu.unisep.jobs_api.response.DefaultResponse;
import lombok.AllArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@AllArgsConstructor
@RestController
@RequestMapping("/job")
public class JobController {

    private final FindAllJobsUseCase findAllJobs;

    @GetMapping
    public ResponseEntity<DefaultResponse<List<JobDto>>> findAll() {
        var jobs = findAllJobs.execute();

        return jobs.isEmpty() ?
                ResponseEntity.noContent().build():
                ResponseEntity.ok(DefaultResponse.of(jobs));
    }
}
