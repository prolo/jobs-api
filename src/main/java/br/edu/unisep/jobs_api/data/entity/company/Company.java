package br.edu.unisep.jobs_api.data.entity.company;

import br.edu.unisep.jobs_api.data.entity.industry.Industry;
import lombok.Data;

import javax.persistence.*;

@Data
@Entity
@Table(name = "companies")
public class Company {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "company_id")
    private Integer id;

    @Column(name = "name")
    private String name;

    @OneToOne
    @JoinColumn(name = "industry_id", referencedColumnName = "industry_id")
    private Industry industry;
}
