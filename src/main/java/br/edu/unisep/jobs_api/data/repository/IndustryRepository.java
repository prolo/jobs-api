package br.edu.unisep.jobs_api.data.repository;

import br.edu.unisep.jobs_api.data.entity.industry.Industry;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface IndustryRepository extends JpaRepository<Industry, Integer> {
}
