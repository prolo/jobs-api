package br.edu.unisep.jobs_api.data.entity.job;

import br.edu.unisep.jobs_api.data.entity.company.Company;
import lombok.Data;

import javax.persistence.*;

@Data
@Entity
@Table(name = "jobs")
public class Job {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "job_id")
    private Integer id;

    @Column(name = "title")
    private String title;

    @Column(name = "description")
    private String description;

    @Column(name = "seniority")
    private String seniority;

    @Column(name = "modality")
    private String modality;

    @Column(name = "salary")
    private String salary;

    @OneToOne
    @JoinColumn(name = "company_id", referencedColumnName = "company_id")
    private Company company;
}
