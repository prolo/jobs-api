package br.edu.unisep.jobs_api.data.repository;

import br.edu.unisep.jobs_api.data.entity.job.Job;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface JobRepository  extends JpaRepository<Job, Integer> {
}
