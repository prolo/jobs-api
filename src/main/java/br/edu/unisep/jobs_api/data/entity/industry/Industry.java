package br.edu.unisep.jobs_api.data.entity.industry;

import lombok.Data;

import javax.persistence.*;

@Data
@Entity
@Table(name = "industries")
public class Industry {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "industry_id")
    private Integer id;

    @Column(name = "name")
    private String name;
}
