package br.edu.unisep.jobs_api.data.repository;

import br.edu.unisep.jobs_api.data.entity.company.Company;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface CompanyRepository  extends JpaRepository<Company, Integer> {
}
